# Image processing assignment

The main code is in `classifier.ipynb`, the data is also present before and after splitting

This is the final result:

#### Without image processing
![plot without image process](./plot_without_image_processing.png)

#### With image processing
![plot with image process](./plot_with_image_processing.png)
